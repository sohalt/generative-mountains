using UnityEditor;
using UnityEngine;

namespace GenerativeMountains
{
    [CustomEditor(typeof(MyLowPolyTerrainGeneratorConfigurator))]
    public class MyLowPolyTerrainGeneratorConfiguratorEditor : UnityEditor.Editor
    {
        private MyLowPolyTerrainGeneratorConfigurator generator;

        private void OnEnable()
        {
            generator = (MyLowPolyTerrainGeneratorConfigurator) target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space();
            if (GUILayout.Button("Generate mesh"))
            {
                Undo.RecordObjects(new Object[]
                {
                    generator,
                    generator.terrainMeshFilter,
                }, "Generate terrain");
                generator.Generate(randomizeConfig: false);
            }
            if (GUILayout.Button("Randomize config and generate mesh"))
            {
                Undo.RecordObjects(new Object[]
                {
                    generator,
                    generator.terrainMeshFilter,
                }, "Generate terrain");
                generator.Generate(randomizeConfig: true);
            }
        }
    }
}
